import speech_recognition as sr
from deep_translator import GoogleTranslator
import os
from gtts import gTTS
import playsound
from tkinter import *
from tkinter import ttk
from googletrans import Translator, LANGUAGES
from tkinter import messagebox

ll = GoogleTranslator().get_supported_languages(as_dict=True)

language = list(ll.keys())

def text():
    t = Input_text.get(1.0, END) 
    d = dest_lang.get()

    translated = GoogleTranslator(source='auto', target=d).translate(t)
    
    Output_text.delete(1.0, END)
    Output_text.insert(END, translated)

def voice():
    r = sr.Recognizer()
    mic = sr.Microphone()
    d = dest_lang.get()

    with mic as source:
        messagebox.showinfo(title='Info',message="Listening...")
        r.adjust_for_ambient_noise(source)  # , duration=int(1))
        r.energy_threshold = 100
        r.pause_threshold = 0.5
        audio = r.listen(source)
        print("\nWait a second .")
        query = ''
        try:
            messagebox.showinfo(title='Info',message="Recognizing...")
            query = r.recognize_google(audio, language="en-in")
            print("\nuser Said :- ", query)
            translated = GoogleTranslator(source='auto', target=d).translate(query)
            Output_text.delete(1.0, END)
            Output_text.insert(END, translated)
        except Exception as e:
            print(e)
            messagebox.showinfo(title='Info',message="Not Recognised")
def ttvoice():
    t = Input_text.get(1.0, END) 
    dl = dest_lang.get()
    d = ll[dest_lang.get()]

    translated = GoogleTranslator(source='auto', target=d).translate(t)
    #output = gTTS(text = translated, lang = d, slow = False)
    
    # Save the audio file to a temporary file
    out = "translated.mp3"
    output.save("translated.mp3")
    
    Output_text.delete(1.0, END)
    Output_text.insert(END, translated)

    # Play the audio file
    playsound.playsound('translated.mp3',True)

def vtvoice():
    r = sr.Recognizer()
    mic = sr.Microphone()
    dl = dest_lang.get()
    d = ll[dest_lang.get()]

    with mic as source:
        messagebox.showinfo(title='Info',message="Listening...")
        r.adjust_for_ambient_noise(source)  # , duration=int(1))
        r.energy_threshold = 100
        r.pause_threshold = 0.5
        audio = r.listen(source)
        print("\nWait a second .")
        query = ''
        try:
            messagebox.showinfo(title='Info',message="Recognizing...")
            query = r.recognize_google(audio, language="en-in")
            print("\nuser Said :- ", query)
            
            Input_text.delete(1.0, END)
            Input_text.insert(END, query)
            
            translated = GoogleTranslator(source='auto', target=d).translate(query)
            dest_lang.get()
            output = gTTS(text = translated, lang = d, slow = False)
            
            # Save the audio file to a temporary file
            out = "translated.mp3"
            output.save("translated.mp3")

            Output_text.delete(1.0, END)
            Output_text.insert(END, translated)
            # Play the audio file
            playsound.playsound('translated.mp3',True)
        except Exception as e:
            print(e)
            messagebox.showinfo(title='Info',message="Not Recognised")
if __name__ == "__main__":
    root = Tk()
    root.geometry('1080x850')
    root.resizable(0,0)
    root.config(bg = 'ghost white')
    root.title("G63")
    Label(root, text = "LANGUAGE TRANSLATOR", font = "arial 20 bold", bg='white smoke').pack()
    Label(root,text ="Enter Text", font = 'arial 13 bold', bg ='white smoke').place(x=200,y=60)
    Input_text = Text(root,font = 'arial 10', height = 11, wrap = WORD, padx=5, pady=5, width = 60)
    Input_text.place(x=30,y = 100)
    Label(root,text ="Output", font = 'arial 13 bold', bg ='white smoke').place(x=780,y=60)
    Output_text = Text(root,font = 'arial 10', height = 11, wrap = WORD, padx=5, pady= 5, width =60)
    Output_text.place(x = 600 , y = 100)
    src_lang = ttk.Combobox(root, values= language, width =22)
    src_lang.place(x=20,y=60)
    src_lang.set(language[27])
    dest_lang = ttk.Combobox(root, values= language, width =22)
    dest_lang.place(x=890,y=60)
    dest_lang.set(language[45])
    trans_btn = Button(root, text = 'Translate Text to Text',font = 'arial 12 bold',pady = 5,command = text , bg = 'royal blue1', activebackground = 'sky blue')
    trans_btn.place(x = 90, y= 380 )
    trans_btn = Button(root, text = 'Translate Voice to Text',font = 'arial 12 bold',pady = 5,command = voice , bg = 'royal blue1', activebackground = 'sky blue')
    trans_btn.place(x = 300, y= 380 )
    trans_btn = Button(root, text = 'Translate Text to Voice',font = 'arial 12 bold',pady = 5,command = ttvoice , bg = 'royal blue1', activebackground = 'sky blue')
    trans_btn.place(x = 550, y= 380 )
    trans_btn = Button(root, text = 'Translate Voice to Voice',font = 'arial 12 bold',pady = 5,command = vtvoice , bg = 'royal blue1', activebackground = 'sky blue')
    trans_btn.place(x = 800, y= 380 )
    root.mainloop()
